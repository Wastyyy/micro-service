# Micro-Service


# Site de Vente d'Articles d'Occasion

Ce fichier README fournit des informations sur le projet de développement d'un site de vente d'articles d'occasion entre particuliers, en utilisant une architecture en microservices. Dans le cadre de ce projet, On est responsable du développement de la partie "Identité du Vendeur" en utilisant Python comme langage de programmation.

## Description du projet

Le projet vise à créer une plateforme en ligne permettant aux utilisateurs de vendre des articles d'occasion. Les utilisateurs peuvent s'inscrire en tant que vendeurs sur le site et publier leurs articles à vendre. Les acheteurs peuvent parcourir les articles, contacter les vendeurs et effectuer des transactions.

### Inscription d'un utilisateur

#### Route : `/users/register` (méthode POST)

Cette route permet l'inscription d'un utilisateur en tant que vendeur sur le site. Les données d'inscription sont envoyées au format JSON dans le corps de la requête. Les informations requises pour l'inscription sont les suivantes :

- `lastname` : Nom de famille de l'utilisateur
- `firstname` : Prénom de l'utilisateur
- `birthdate` : Date de naissance de l'utilisateur
- `email` : Adresse e-mail de l'utilisateur
- `password` : Mot de passe de l'utilisateur

Lors de l'inscription, les étapes suivantes sont effectuées :

1. Vérification de l'existence de l'utilisateur dans la base de données. Si l'utilisateur existe déjà, une erreur est renvoyée avec le message "User already exists".
2. Création d'un nouvel utilisateur avec les données fournies.
3. Ajout du nouvel utilisateur dans la base de données.
4. Confirmation de l'inscription avec le message "User registered successfully".

### Connexion d'un utilisateur

#### Route : `/users/login` (méthode POST)

Cette route permet la connexion d'un utilisateur en tant que vendeur sur le site. Les informations de connexion sont envoyées au format JSON dans le corps de la requête. Les informations requises pour la connexion sont les suivantes :

- `email` : Adresse e-mail de l'utilisateur
- `password` : Mot de passe de l'utilisateur

Les étapes effectuées lors de la connexion :

1. Recherche de l'utilisateur dans la base de données en utilisant l'adresse e-mail fournie.
2. Vérification de l'existence de l'utilisateur et de la correspondance du mot de passe. Si l'utilisateur n'existe pas ou si le mot de passe est incorrect, une erreur est renvoyée avec le message "Invalid credentials".
3. Confirmation de la connexion avec le message "Login successful".

### Gestion des erreurs

Le fichier Python contient également des gestionnaires d'erreurs pour les cas où une route n'est pas définie (`404 - Not found`) ou en cas d'erreur interne du serveur (`500 - Internal server error`).


## Technologies utilisées

- Python
- Framework Flask
- Base de données (SQLite)
- Bibliothèques tierces (Flask-Login, Flask-WTForms, etc.)

## Testez l'API

Vous pouvez tester l'API en utilisant un outil tel que cURL, Postman ou un client HTTP. 

Démarrez l'api avec la commande : python ./User.py

Pour l'inscription d'un utilisateur, envoyez une requête POST à l'URL http://localhost:5000/users/register avec les données JSON suivantes dans le corps de la requête :
{
  "lastname": "Doe",
  "firstname": "John",
  "birthdate" : "11/07/1994",
  "email": "johndoe@example.com",
  "password": "password123"
}
Pour la connexion d'un utilisateur, envoyez une requête POST à l'URL http://localhost:5000/users/login avec les données JSON suivantes dans le corps de la requête :
{
  "email": "johndoe@example.com",
  "password": "password123"
}


## Etapes d'utilisation des données

1.Créez un environnement virtuel.

    python -m venv venv

2.Activez l'environnement virtuel.

    source venv/bin/activate

3.Installez les dépendances du projet.

    pip install -r requirements.txt

3.Configurez les variables d'environnement requises dans le fichier .env. Initialisez la base de données.

    flask db init
    flask db migrate
    flask db upgrade

4.Démarrez le serveur de développement.
    
    flask run

## Ressources supplémentaires

    Documentation officielle de Python : https://docs.python.org/
    Documentation officielle de Flask : https://flask.palletsprojects.com/
    Tutoriels Flask sur Real Python : https://realpython.com/tutorials/flask/
