from flask import Flask, request, jsonify, session
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///users.db' # Configuration de l'URI de la base de données
db = SQLAlchemy(app)

# Définition du modèle de données pour les utilisateurs
# lastname: "string",
# firstname: "string",
# birthdate: "string",
# email: "string",
# password": "string"

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    lastname = db.Column(db.String(50))
    firstname = db.Column(db.String(50))
    email = db.Column(db.String(50), unique=True)
    birthdate = db.Column(db.String(50))
    password = db.Column(db.String(50))

# Route pour l'inscription d'un utilisateur
# créer un nouvel utilisateur dans la base de données avec les données reçues de type JSON
@app.route('/users/register', methods=['POST'])
def register():
    lastname = request.json['lastname']
    firstname = request.json['firstname']
    birthdate = request.json['birthdate']
    email = request.json['email']
    password = request.json['password']

    # vérifie si l'utilisateur existe déjà dans la base de données
    try:
        new_user = User(lastname=lastname, firstname=firstname, birthdate=birthdate, email=email, password=password)
        db.session.add(new_user)
        db.session.commit()
        return jsonify({'message': 'User registered successfully'})
    except IntegrityError:
        db.session.rollback()
        return jsonify({'error': 'User already exists'}), 409

# Route pour la connexion d'un utilisateur
# vérifie si l'utilisateur existe dans la base de données et si le mot de passe est correct
@app.route('/users/login', methods=['POST'])
def login():
    email = request.json['email']
    password = request.json['password']

    user = User.query.filter_by(email=email).first()

    # vérifie si l'utilisateur existe dans la base de données et si le mot de passe est correct
    if user and user.password == password:
        session['user_id'] = user.id  # Stockage de l'ID de l'utilisateur dans la session
        return jsonify({'message': 'Login successful'})
    else:
        return jsonify({'error': 'Invalid credentials'}), 401

# Route pour obtenir les informations de l'utilisateur connecté
@app.route('/users/me', methods=['GET'])
def get_user_info():
    user_id = session.get('user_id')  # Récupération de l'ID de l'utilisateur depuis la session

    # vérifie si l'utilisateur est connecté
    # s'il est connecté, renvoie les informations de l'utilisateur
    if user_id:
        user = User.query.get(user_id)
        return jsonify({
            'id': user.id,
            'lastname': user.lastname,
            'firstname': user.firstname,
            'birthdate': user.birthdate,
            'email': user.email
        })
    else:
        return jsonify({'error': 'Not authenticated'}), 401

# message d'erreur pour les routes non définies
@app.errorhandler(404)
def not_found_error(error):
    return jsonify({'error': 'Not found'}), 404

# message d'erreur pour les erreurs internes au serveur
@app.errorhandler(500)
def internal_error(error):
    return jsonify({'error': 'Internal server error'}), 500

# création de la base de données
with app.app_context():
    db.create_all()

# lancement de l'application
if __name__ == '__main__':
    app.run()
